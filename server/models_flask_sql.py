from core_app import db, ma


class Grilla(db.Model):
    __tablename__ = 'grillas'
    id = db.Column(db.Integer, primary_key=True)
    x = db.Column(db.Integer)
    y = db.Column(db.Integer)
    w = db.Column(db.Integer)
    h = db.Column(db.Integer)
    i = db.Column(db.String(10))




class Tarea(db.Model):
    __tablename__ = 'tareas'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    responsible = db.Column(db.String(50))
    description = db.Column(db.String(50))
    priority = db.Column(db.Integer)
    GrillaId =db.Column(db.Integer,db.ForeignKey('grillas.id'))
    grilla = db.relationship('Grilla', backref='tareas')


class GrillaSchema(ma.ModelSchema):
    class Meta:
        model = Grilla

class TareaSchema(ma.ModelSchema):
    class Meta:
        model = Tarea
    grilla = ma.Nested(GrillaSchema)

