import urllib

from flask import jsonify
from flask import request

from core_app import app
from flask_cors import CORS
from models_flask_sql import Tarea, TareaSchema, GrillaSchema, Grilla


# @app.route('/', methods=['POST', 'GET'])
# def login():
#     movies = session.query(Tarea).all()
#     return jsonify(movies)


@app.route('/api/get_tareas', methods=['POST', 'GET'])
def get_tareas():
    tareas = TareaSchema(many=True).dump(Tarea.query.all()).data
    grillas = GrillaSchema(many=True).dump(Grilla.query.all()).data
    return jsonify({'tareas':tareas,'grillas':grillas})


@app.route('/api/set_tarea', methods=['POST', 'GET'])
def set_tareas():
    tarea = TareaSchema()
    tarea_obj= tarea.load(request.json,session=db.session)
    db.session.add(tarea_obj.data)
    db.session.commit()
    return jsonify(tarea.dump(tarea_obj.data).data)

from models_flask_sql import db
db.create_all()

@app.route('/api/save_pos', methods=['POST', 'GET'])
def save_pos():
    for pos in request.json:
        tarea= Tarea.query.get(pos['i'])
        if tarea.grilla is None:
            tarea.grilla = Grilla()
            db.session.add(tarea.grilla)
        tarea.grilla.x = pos['x']
        tarea.grilla.y = pos['y']
        tarea.grilla.w = pos['w']
        tarea.grilla.h = pos['h']
        tarea.grilla.i = pos['i']
        db.session.commit()
    return jsonify({'res':'exitoso'})



@app.route('/api/del_tarea', methods=['POST', 'GET'])
def del_tarea():
    tarea = Tarea.query.get(request.json['id'])
    if tarea.grilla is not None:
        db.session.delete(tarea.grilla)
    db.session.delete(tarea)
    db.session.commit()
    return jsonify({'id':request.json['id']})



if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True, debug=True,port=50001)
