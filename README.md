# Proyecto de 2 partes
## Server
Backend puerto 4000
*  Si es linux instalar mariadb con los siguientes comandos
```
 sudo apt install mariadb-server
 sudo apt install mariadb-client
 sudo apt install libmariadb-dev
 sudo apt install libmariadb-dev-compat
 sudo apt-get install libmariadbclient18
```


*  Entrar en server
    *  ` cd server`
*  crear virtalenv y entrar en el mismo
*  una vez colocado ejecutar 
    *  `pip install -r requirements.txt`
*  preparar base de datos
    * motor: mariadb
    * nombre base: tareas_charts
    * usuario:root
    * password:1234
*  ejecutar creador inicial
    * `python db_crearBase.py`
*  ejecutar server
    * `python server.py`


## Cliente
Frontend puerto 3000
*  Entrar en cliente
    *  ` cd cliente`
*  instalar todo 
    *  `npm install`
*  ejecutar cliente
    * `npm start`
