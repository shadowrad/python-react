import React, { Component } from 'react';
import './App.css';
import TareasGrid from './components/TareasGrid'

class App extends Component {
  constructor() {
    super();
  }


  render() {
    return (
      <div className="App">
          <div className="container">
        <TareasGrid />
        </div>
      </div>
    );
  }
}

export default App;