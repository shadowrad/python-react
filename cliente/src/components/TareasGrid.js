import React, { Component } from 'react';
import GridLayout from 'react-grid-layout';
import PlotApp from './plot'
// data
import { todos } from './todos.json';

// subcomponents
import TodoForm from './TodoForm';

class TareaGrid extends Component {
  constructor() {
    super();
    this.API_URL='http://35.245.127.92:50001';
    fetch(this.API_URL+'/api/get_tareas').then(results=>results.json()
    ).then(
        (result) => {
          this.setState({
            todos: result.tareas,
            layout : result.grillas
          });
        });
    this.state = {
      todos:[],
      layout :[]
    }
    this.handleAddTodo = this.handleAddTodo.bind(this);
    this.handleSaveGrid = this.onLayoutChange.bind(this);
    //this.API_URL='http://35.245.127.92:50001';
  }


  removeTodo(id) {   
    fetch(this.API_URL+'/api/del_tarea', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({'id':id})
    }).then(res => res.json())
         .then(
            (result) => {
              this.setState({
                todos: this.state.todos.filter((e, i) => {
                  return e.id !== id
                })
              });
        }
    );
  }

  savePosition_single(id) {
    var layout_seleccionada = {}   
    this.state.layout.map((data) => {
    if (data.i === id){
      layout_seleccionada = data;
    }
    });

    fetch(this.API_URL+'/api/save_pos', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({'id':id,'layout':layout_seleccionada})
    }).then(res => res.json());
  }

  savePositions() {   
    fetch(this.API_URL+'/api/save_pos', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(this.state.layout)
    }).then(res => {
      console.log("salvado");
    }
      ).catch(error => {
      console.log(error);
  });;
  }


  handleAddTodo(todo) {
    fetch(this.API_URL+'/api/set_tarea', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(todo)
    }).then(res => res.json())
         .then(
            (result) => {
              this.setState({
                todos: [...this.state.todos, result]
              })
        }
    );
  }


  onLayoutChange(layout) {
    /*eslint no-console: 0*/
    //saveToLS("layout", layout);
    this.setState({ layout });
    console.log('cambio: '+JSON.stringify(layout))
    //this.props.onLayoutChange(layout); // updates status display
  }

  render() {
    const todos = this.state.todos.map((todo, i) => {
      var grilla = {w: 4, h: 1, 'x': 0, 'y': i, i: todo.id}
      if (todo.Grilla != null)
          grilla = {x: todo.Grilla.x, y:todo.Grilla.y, w: todo.Grilla.w, h: todo.Grilla.h, i:todo.Grilla.i}
         console.log('dibujo en data grid:'+JSON.stringify(grilla))
         return (
      <div key={todo.id} >
      <div  
          data-grid={grilla}  
          style= {{width: "100%", height: "100%"}}  >
              <h3>{todo.title}</h3>
              <div 
               style= {{width: "100%", height: "70%" }}
              ><PlotApp tipo={todo.priority} />
              </div>
              <div  style= {{height: "10%" }} >    
              <button
                className="btn btn-danger"
                onClick={this.removeTodo.bind(this, todo.id)}>
                Delete
              </button>

              </div>        
                        </div>
      
      
      
      </div>
      )
    });

    // RETURN THE COMPONENT
    return (
         <div className=" mt-2">
            <div className="col-md-2 text-center">
              <TodoForm onAddTodo={this.handleAddTodo}></TodoForm>
            </div>
            <div className="col-md-8">
            <button
                className="btn btn-success"
                onClick={this.savePositions.bind(this)}
                >
                guardar grilla
              </button>
            <GridLayout className="layout" 
            layout={this.state.layout}
            onLayoutChange ={this.handleSaveGrid}
            cols={12} rowHeight={400} colWidth={400}  width={2200} Height={2200}> 
                {todos}
              </GridLayout>
            </div>
          </div>
    );
  }
}

export default TareaGrid;