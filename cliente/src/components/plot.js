import React from 'react';
import createPlotlyComponent from 'react-plotlyjs';
//See the list of possible plotly bundles at https://github.com/plotly/plotly.js/blob/master/dist/README.md#partial-bundles or roll your own
import Plotly from 'plotly.js/dist/plotly-basic';
const PlotlyComponent = createPlotlyComponent(Plotly);


class PlotApp extends React.Component {

  render() {

    
   
    const rand = () => Math.random();
    var x = [1, 2, 3, 4, 5];
    const new_data = (trace) => Object.assign(trace, {y: x.map(rand)});
  var data_lines = [
	{mode:'lines', line: {color: "#b55400"}},
	{mode: 'lines', line: {color: "#393e46"}},
	{mode: 'lines', line: {color: "#222831"}}
].map(new_data);

var data_bar = [
  {
    x: ['giraffes', 'orangutans', 'monkeys'],
    y: [20, 14, 23],
    type: 'bar'
  }
];

var data_pie = [{
  values: [19, 26, 55],
  labels: ['Residential', 'Non-Residential', 'Utility'],
  type: 'pie'
}];

var tipos = ['bar','lineas','pie']
var datos = [data_bar,data_lines,data_pie]

var layout = {
  title: 'ejemplo '+ tipos[this.props.tipo-1],
  uirevision:'true',
  autosize: true,
	xaxis: {autorange: true},
	yaxis: {autorange: true}
};


	// user interation will mutate layout and set autorange to false
	// so we need to reset it to true
	layout.xaxis.autorange = true;
	layout.yaxis.autorange = true;

    return (
      <PlotlyComponent
        data={datos[this.props.tipo-1]}
        layout={ layout }
        useResizeHandler={true}
        responsive={true}
        style= {{width: "100%", height: "90%"}}
      />
    );
  }
}

export default PlotApp;
