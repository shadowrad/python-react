import React, { Component } from 'react';
import { parse } from 'url';

class TodoForm extends Component {
  constructor () {
    super();
    this.state = {
      title: 'titulo 1',
      responsible: 'rocco',
      description: 'pruebas insert',
      priority: 2
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.onAddTodo(this.state);

    num_str = this.state.title.substring(this.state.title.length-2); //gets the substring from index position 3 to the end
    var num = parseInt(num_str);    
    var num_str= num.toString()

    this.setState({
      title: this.state.title.replace(num_str,(num+1).toString()),
      responsible: this.state.responsible,
      description: this.state.description,
      priority: this.state.priority
    });
  }

  handleInputChange(e) {
    const {value, name} = e.target;
    console.log(value, name);
    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div className="card">
        <form onSubmit={this.handleSubmit} className="card-body">
          <div className="form-group">
            <input
              type="text"
              name="title"
              className="form-control"
              value={this.state.title}
              onChange={this.handleInputChange}
              placeholder="Title"
              />
          </div>
          <div className="form-group">
            <input
              type="text"
              name="responsible"
              className="form-control"
              value={this.state.responsible}
              onChange={this.handleInputChange}
              placeholder="Responsible"
              />
          </div>
          <div className="form-group">
            <input
              type="text"
              name="description"
              className="form-control"
              value={this.state.description}
              onChange={this.handleInputChange}
              placeholder="Description"
              />
          </div>
          <div className="form-group">
            <select
                name="priority"
                className="form-control"
                value={this.state.priority}
                onChange={this.handleInputChange}
              >
              <option value="1" >bar</option>
              <option value="2">linea</option>
              <option value="3">pie</option>
            </select>
          </div>
          <button type="submit" className="btn btn-primary">
            Save
          </button>
        </form>
      </div>
    )
  }

}

export default TodoForm;